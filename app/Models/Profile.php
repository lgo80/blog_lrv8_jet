<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Profile extends Model
{
    use HasFactory;

    public function user(){
        /*
        Esto es igual a lo de hasOne
        $profile = Profile::where('user_id', $this->id)->first();
        return $profile;
        */
        return $this->belongsTo(User::class);
    }
}
