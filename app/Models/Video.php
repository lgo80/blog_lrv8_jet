<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Coment;

class Video extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Relacion 1 a muchos polimorfica
    public function coments(){
        return $this->morphMany(Coment::class,'comentable');
    }

    //Relacion muchos a muchos polimorfica
    public function posts(){
        return $this->morphToMany(Tag::class,'taggable');
    }
}
