<x-app-layout>
    <x-slot name="header">
        <h2 class="font-bold text-xl text-blue-800 leading-tight">
            Coders Free
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus maiores ut quis architecto. Voluptatibus quas quae deserunt modi pariatur voluptatem temporibus rerum perspiciatis dolorum placeat, minus quisquam beatae? Nam, repellendus!</p>
                <p>Impedit in magnam quis molestiae nostrum! Dolorem ratione nesciunt odio quaerat dolor enim dignissimos in molestias sunt ipsam? Distinctio, beatae nisi. Autem eum dolor fugit voluptatem vitae nobis, necessitatibus iste?</p>
                <p>Provident nihil dolor illo dolorem repellat! Voluptatum voluptatibus adipisci eum, consequuntur necessitatibus illo optio unde rerum temporibus laboriosam facere placeat, culpa suscipit repellat fuga voluptates vitae omnis! At, libero quia.</p>
                <p>Sed possimus fuga sunt, quod praesentium veritatis, expedita repellendus accusantium labore ipsum delectus tempora nulla non velit doloribus dolor natus doloremque reiciendis necessitatibus architecto temporibus voluptatum maxime. Aliquid, modi optio.</p>
                <p>Facilis dolorem dolore porro eligendi magni voluptate nulla id alias, fugit voluptatum! Inventore non error aliquam accusantium exercitationem dolorem pariatur qui, doloribus ex officiis, ipsum quidem voluptate temporibus! Commodi, nihil?</p>
            </div>
        </div>
    </div>
</x-app-layout>
